//
//  ViewController.swift
//  FirebaseDemo
//
//  Created by AVINASH on 19/03/21.
//  Copyright © 2021 AVINASH. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {
    
    
    var ref : DatabaseReference?
    var databaseHandle :DatabaseHandle?
    var postData = [String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        
        // writing data to firebase database
        
       //  ref?.child("MyPost").childByAutoId().setValue("Abhishek")
        
       //  ref?.child("MyPost").child("admin").setValue("Abhishek")
        
        // Do any additional setup after loading the view.
        
        // retriving data from Firebase
        
        
        
      /*  databaseHandle =*/
//        ref?.child("MyPost").observeSingleEvent(of: .value, with: { (snapshot) in
//               let post = snapshot.value as? String
//
//                     if let actualPost = post{
//
//                         print(">>>>>\(actualPost)")
//
//                         self.postData.append(actualPost)
//                     }
//        })
        
       databaseHandle = ref?.child("MyPost").observe(.childAdded, with: { (snapshot) in
            
            // just modified by avinash singh
            
          //  print(snapshot)
            
            let post = snapshot.value as? String
            
            if let actualPost = post{
                
                print(">>>>>\(actualPost)")
                
                self.postData.append(actualPost)
            }
            
        
            
        })
        
        
    }


}

